package ca.sb.buttons.generators;

import java.util.*;

import ca.sb.buttons.scores.*;


public class ScoreGenerator
{
  Random random;
  private final int MAGIC = 6500000;
  
  public ScoreGenerator()
  {
    random = new Random();
  }
  public static int randBetween(int start, int end) {
    return start + (int)Math.round(Math.random() * (end - start));
  }
  public Score getSingleScore()
  {
    final int MAX = 100;
    int success = random.nextInt(MAX);
    int total =  success + random.nextInt(MAX);
    //long date = (long)Math.abs(random.nextLong() / MAGIC);
    GregorianCalendar gc = new GregorianCalendar();

    int year = randBetween(1970, 2014);

    gc.set(gc.YEAR, year);

    int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));

    gc.set(gc.DAY_OF_YEAR, dayOfYear);
    
    // Prevents dates over Feb 2014, 14 being created
    if( gc.get(gc.YEAR) == 2014 &&
    		gc.get(gc.MONTH) == gc.FEBRUARY &&
    		gc.get(gc.DAY_OF_MONTH) > 14)
    	gc.set(gc.DAY_OF_MONTH, 14);

    long date = gc.getTimeInMillis();
    
    return new Score(success, total, date);
  }
  
  public List<Score> getListOfScores(int total)
  {
    List<Score> list = new ArrayList<Score>(total);
    
    for (int pos = 0; pos < total; pos++)
    {
      list.add(getSingleScore());
    }
    
    return list;
  }
  

}
