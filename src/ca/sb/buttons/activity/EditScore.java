package ca.sb.buttons.activity;

import java.util.GregorianCalendar;

import com.activeandroid.query.Select;

import ca.sb.buttons.R;
import ca.sb.buttons.R.layout;
import ca.sb.buttons.R.menu;
import ca.sb.buttons.scores.Score;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v4.app.NavUtils;

public class EditScore extends Activity {
	boolean valid = false;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_score);
		// Show the Up button in the action bar.
		setupActionBar();
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.edit_score, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	EditText scoresinput;
	EditText triesinput;
	DatePicker datepicker;
	public void onValidateClicked(View v)
	{
		scoresinput = (EditText)findViewById(R.id.editText1);
		triesinput = (EditText)findViewById(R.id.editText2);
		datepicker = (DatePicker)findViewById(R.id.datePicker1);
		
		int inputscores = Integer.parseInt(scoresinput.getText().toString());
		int inputtries  = Integer.parseInt(triesinput.getText().toString());
		
		if(inputscores > inputtries)
		{
			Toast.makeText(this, "Score can not be higher than tries", Toast.LENGTH_SHORT)
      .show();
			return;
		}
		
		if(datepicker.getYear() < 1970 || datepicker.getYear() > 2014)
		{
			Toast.makeText(this, "Date is not in valid range", Toast.LENGTH_SHORT)
      .show();
			return;
		}
		if(datepicker.getDayOfMonth() > 14 && datepicker.getYear() >= 2014)
		{
			Toast.makeText(this, "Date can not be higher than Feb 14, 2014",
					Toast.LENGTH_SHORT).show();
			return;
		}
		valid = true;
		Toast.makeText(this, "Score is Valid",
				Toast.LENGTH_SHORT).show();
	}
	public void onUpdateClicked(View v)
	{
		long id = getIntent().getExtras().getLong("id");
		Score score = new Select().from(Score.class).where("Id = ?", id).executeSingle();
		
		score.success = Integer.parseInt(scoresinput.getText().toString());
		score.total = Integer.parseInt(triesinput.getText().toString());
		GregorianCalendar gc = new GregorianCalendar();
		gc.set(gc.YEAR, datepicker.getYear());
		gc.set(gc.MONTH, datepicker.getMonth());
		gc.set(gc.DAY_OF_MONTH, datepicker.getDayOfMonth());
		score.date = gc.getTimeInMillis();
		
		if(valid)
			score.save();
	}
}
