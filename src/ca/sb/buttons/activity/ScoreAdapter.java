package ca.sb.buttons.activity;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.activeandroid.query.Delete;
import ca.sb.buttons.scores.Score;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScoreAdapter extends ArrayAdapter<Score>
{
	private List<Score> list;
	private Activity context;

	public ScoreAdapter(Activity context, List<Score> list)
	{
		super(context, ca.sb.buttons.R.layout.score_list_item, list);
		this.context = context;
		this.list = list;
	}

	@Override
	public int getCount()
	{
		return list.size();
	}

	@Override
	public Score getItem(int position)
	{
		return list.get(position);
	}

	@Override
	public long getItemId(int position)
	{
		return position;
	}

	static class ViewHolder
	{
		//public TextView textViewScoreId;
		public TextView textViewScoreSuccess;
		public TextView textViewScoreTotal;
		public TextView textViewScoreDate;
		public TextView textViewScoreRatio;
		public Button buttonUpdate;
		public Button buttonDelete;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent)
	{
		final ViewHolder holder;

		if (convertView == null)
		{
			LayoutInflater inflator = context.getLayoutInflater();
			convertView = inflator.inflate(ca.sb.buttons.R.layout.score_list_item,
					null);
			// --------------------------------------------------
			holder = new ViewHolder();
			holder.textViewScoreSuccess = (TextView) convertView
					.findViewById(ca.sb.buttons.R.id.textViewScoreSuccess);
			holder.textViewScoreTotal = (TextView) convertView
					.findViewById(ca.sb.buttons.R.id.textViewScoreTotal);
			holder.textViewScoreDate = (TextView) convertView
					.findViewById(ca.sb.buttons.R.id.textViewScoreRatio);
			holder.textViewScoreRatio = (TextView) convertView
					.findViewById(ca.sb.buttons.R.id.textViewScoreDate);
			holder.buttonDelete = (Button) convertView
					.findViewById(ca.sb.buttons.R.id.buttonDelete);
			holder.buttonUpdate = (Button) convertView
					.findViewById(ca.sb.buttons.R.id.buttonUpdate);

			// -------------------------------------------------
			convertView.setTag(holder);
		}
		else
		{
			holder = (ViewHolder) convertView.getTag();
		}
		// ------------------------------------------
		final Score score = list.get(position);
		
		holder.textViewScoreSuccess.setText("" + list.get(position).getiSuccess());
		holder.textViewScoreTotal.setText("" + list.get(position).getiTotal());

		holder.textViewScoreDate.setText(""
				+ new SimpleDateFormat("MM/dd/yyyy").format(new Date(list.get(position)
						.getDate())));
		holder.buttonDelete.setTag(score);
		holder.buttonUpdate.setTag(score);

		DecimalFormat format = new DecimalFormat("##.##");
		holder.textViewScoreRatio.setText(""
				+ format.format(list.get(position).getRatio()));

		holder.buttonDelete.setOnClickListener(new View.OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				Score score = (Score) v.getTag();
				int pos = list.indexOf(score);

				if (pos >= 0)
					list.remove(pos);

				if (score != null)
					new Delete().from(Score.class).where("Id = ?", score.getId()).executeSingle();

				notifyDataSetChanged();
			}
		});
		holder.buttonUpdate.setOnClickListener(new View.OnClickListener()
		{
			
			@Override
			public void onClick(View v) {
        Score score = (Score) v.getTag();
        // Launching new Activity on selecting single List Item
        Intent intent = new Intent(context, EditScore.class);
        // sending data to new activity
        Bundle bundle = new Bundle();
        bundle.putLong("id", score.getId());
        intent.putExtras(bundle);
        context.startActivity(intent);				
			}
		});
		return convertView;
	}
}
